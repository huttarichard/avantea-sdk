(function() {
  var __hasProp = {}.hasOwnProperty,
    __extends = function(child, parent) { for (var key in parent) { if (__hasProp.call(parent, key)) child[key] = parent[key]; } function ctor() { this.constructor = child; } ctor.prototype = parent.prototype; child.prototype = new ctor(); child.__super__ = parent.prototype; return child; };

  this.Avantea = (function(simpleStorage, PubSub, _, Deferred) {
    var App;
    App = {};
    App.VERSION = '0.0.2';
    App.development = false;
    App.Deferred = Deferred;
    App.storage = simpleStorage;
    App.events = PubSub;
    App.stringNamespace = function(ns) {
      var i, l, n, o;
      o = void 0;
      n = ns.split(".");
      o = App[n[0]] = App[n[0]] || {};
      l = n.length;
      i = 1;
      while (i < l) {
        o = o[n[i]] = o[n[i]] || {};
        i++;
      }
      return o;
    };
    App.module = function(name, fn) {
      var namespace;
      namespace = name.split('.').length > 1 ? App.stringNamespace(name) : (App[name] || (App[name] = {}), App[name]);
      return fn.call(App, namespace, App, _, PubSub);
    };
    return App;
  })(simpleStorage, PubSub, _, Deferred);

  this.Avantea.module("Sdk", function(Sdk, App, _, events) {
    return Sdk.XHR = (function() {
      function XHR(method, url) {
        var reject, resolve;
        this.xhr = new XMLHttpRequest;
        this.deferred = new Deferred;
        resolve = _.bind(this.deferred.resolve, this.deferred, this.xhr);
        reject = _.bind(this.deferred.reject, this.deferred, this.xhr);
        this.xhr.open(method, url, true);
        this.xhr.addEventListener("load", function(event) {
          var xhr;
          xhr = event.currentTarget;
          if (_.contains([200, 201, 422], xhr.status)) {
            return resolve(xhr);
          } else {
            return reject(xhr);
          }
        });
        this.xhr.addEventListener("error", reject);
        this.xhr.addEventListener("abort", reject);
      }

      XHR.prototype.withTokenAuth = function(key) {
        return this.xhr.setRequestHeader('Authorization', 'Token token="' + key + '"');
      };

      XHR.prototype.send = function() {
        this.xhr.send(this.data);
        return this.deferred;
      };

      XHR.prototype.withJsonData = function(data) {
        this.xhr.setRequestHeader('Content-Type', 'application/json');
        return this.data = JSON.stringify(data);
      };

      XHR.prototype.withData = function(data) {
        return this.data = data;
      };

      return XHR;

    })();
  });

  this.Avantea.module("Sdk", function(Sdk, App, _, events) {
    return Sdk.Storage = (function() {
      function Storage() {}

      Storage.prototype.storage = App.storage;

      Storage.prototype.expires = 1000 * 60 * 60 * 24 * 90;

      Storage.prototype.prefix = 'id';

      Storage.prototype.generateUniqueTimeId = function() {
        return _.uniqueId(this.prefix) + (new Date).getTime();
      };

      Storage.prototype.isCorrectPrefix = function(key) {
        return key.slice(0, this.prefix.length) === this.prefix;
      };

      Storage.prototype.add = function(obj, id) {
        id = id ? this.prefix + id : this.generateUniqueTimeId();
        if (!this.storage.canUse()) {
          this.storage.flush();
        }
        this.storage.set(id, obj, {
          TTL: this.expires
        });
        return id.slice(this.prefix.length, id.length);
      };

      Storage.prototype.get = function(id) {
        return this.storage.get(this.prefix + id);
      };

      Storage.prototype.getAll = function() {
        return this.map();
      };

      Storage.prototype.remove = function(key) {
        return this.storage.deleteKey(this.prefix + key);
      };

      Storage.prototype.clear = function() {
        return this.storage.flush();
      };

      Storage.prototype.count = function() {
        return this.getAll().length;
      };

      Storage.prototype.saveClear = function() {
        return this.each((function(_this) {
          return function(val, key) {
            if (_this.isCorrectPrefix(key)) {
              return _this.remove(key);
            }
          };
        })(this));
      };

      Storage.prototype.iterate = function(iterateFn, callback) {
        var index, isCorrectPrefix;
        if (callback == null) {
          callback = function(val, key) {
            return val;
          };
        }
        isCorrectPrefix = _.bind(this.isCorrectPrefix, this);
        index = _.filter(this.storage.index(), isCorrectPrefix);
        return _(index)[iterateFn]((function(_this) {
          return function(key) {
            var notPrefixed;
            notPrefixed = key.slice(_this.prefix.length, key.length);
            return callback(_this.storage.get(key), notPrefixed);
          };
        })(this));
      };

      Storage.prototype.each = function(callback) {
        return this.iterate("each", callback);
      };

      Storage.prototype.map = function(callback) {
        return this.iterate("map", callback);
      };

      Storage.prototype.filter = function(callback) {
        return this.iterate("filter", callback);
      };

      return Storage;

    })();
  });

  this.Avantea.module("Sdk", function(Sdk, App, _, events) {
    Sdk.RequestStorage = (function(_super) {
      __extends(RequestStorage, _super);

      function RequestStorage() {
        return RequestStorage.__super__.constructor.apply(this, arguments);
      }

      return RequestStorage;

    })(Sdk.Storage);
    return Sdk.Dispatcher = (function() {
      function Dispatcher(namespace, apiKey) {
        this.apiKey = apiKey;
        this.storage = new Sdk.RequestStorage;
        this.storage.prefix = namespace;
      }

      Dispatcher.prototype.addToQueue = function(method, url, payload) {
        return this.storage.add({
          method: method,
          url: url,
          payload: payload
        });
      };

      Dispatcher.prototype.request = function(method, url, payload) {
        return this.send({
          method: method,
          url: url,
          payload: payload
        });
      };

      Dispatcher.prototype.ableToDispatch = function() {
        return this.storage.count() && window.navigator.onLine;
      };

      Dispatcher.prototype.dispatch = function() {
        var deferred, flushed;
        if (!this.ableToDispatch()) {
          return;
        }
        events.publish('dispatcher:start');
        flushed = this.storage.map((function(_this) {
          return function(val, key) {
            var deferred;
            deferred = _this.send(val);
            deferred.done(_.bind(_this.storage.remove, _this.storage, key));
            return deferred;
          };
        })(this));
        deferred = App.Deferred.when.apply(App.Deferred, flushed);
        deferred.done(_.bind(events.publish, events, 'dispatcher:end'));
        return void 0;
      };

      Dispatcher.prototype.send = function(obj) {
        var xhr;
        xhr = new Sdk.XHR(obj.method, obj.url);
        xhr.withTokenAuth(this.apiKey);
        xhr.withJsonData(obj.payload);
        return xhr.send();
      };

      return Dispatcher;

    })();
  });

  this.Avantea.module("Sdk", function(Sdk, App, _, events) {
    Sdk.onlineCallback = function() {
      return events.publish("device:online");
    };
    Sdk.offlineCallback = function() {
      return events.publish("device:offline");
    };
    Sdk.loadCallback = function() {
      return events.publish("device:load");
    };
    Sdk.loadCallback = _.throttle(Sdk.loadCallback, 200);
    window.addEventListener("load", Sdk.loadCallback);
    window.addEventListener('online', Sdk.onlineCallback);
    window.addEventListener('offline', Sdk.offlineCallback);
    document.addEventListener("deviceready", Sdk.loadCallback);
    document.addEventListener("online", Sdk.onlineCallback);
    return document.addEventListener("offline", Sdk.offlineCallback);
  });

  this.Avantea.module("Sdk", function(Sdk, App, _, events) {
    return Sdk.Routes = (function() {
      function Routes() {}

      Routes.host = function(service, protocol) {
        if (protocol == null) {
          protocol = "http";
        }
        if (App.development) {
          return "http://localhost:3000/";
        }
        return protocol + "://" + service + ".avantea.cz/";
      };

      Routes.api = function(service, uri, version) {
        if (version == null) {
          version = "v1";
        }
        return this.host(service) + "api/" + version + "/" + uri;
      };

      return Routes;

    })();
  });

  this.Avantea.module("Statistics", function(Statistics, App, _, events) {
    return Statistics.Routes = (function(_super) {
      __extends(Routes, _super);

      function Routes() {
        return Routes.__super__.constructor.apply(this, arguments);
      }

      Routes.service = "statistiky";

      Routes.event = function(subject, event) {
        return this.api(this.service, subject + "/event/" + event);
      };

      Routes.options = function(subject) {
        return this.api(this.service, subject + "/options");
      };

      Routes.status = function() {
        return this.host(this.service) + 'status';
      };

      return Routes;

    })(App.Sdk.Routes);
  });

  this.Avantea.module("Statistics", function(Statistics, App, _, events) {
    return Statistics.Options = (function(_super) {
      __extends(Options, _super);

      function Options() {
        return Options.__super__.constructor.apply(this, arguments);
      }

      Options.prototype.prefix = 'statistics_option_';

      Options.prototype.defaults = {
        debounce: 1000,
        timeout: 600000
      };

      Options.prototype.assign = function(json) {
        return _.each(this.defaults, (function(_this) {
          return function(val, key) {
            if (!json[key]) {
              return;
            }
            return _this.add(json[key], key);
          };
        })(this));
      };

      Options.prototype.getAll = function() {
        var options;
        options = {};
        _.each(this.defaults, (function(_this) {
          return function(val, key) {
            return options[key] = _this.get(key) || val;
          };
        })(this));
        return options;
      };

      return Options;

    })(App.Sdk.Storage);
  });

  this.Avantea.module("Statistics.Sessions", function(Sessions, App, _, events) {
    Sessions.Storage = (function(_super) {
      __extends(Storage, _super);

      function Storage() {
        return Storage.__super__.constructor.apply(this, arguments);
      }

      Storage.prototype.prefix = 'statistics_session_';

      Storage.prototype.getLastSession = function() {
        return this.get('cache');
      };

      Storage.prototype.create = function(time) {
        this.add(time, 'cache');
        this.add(time, 'hit');
        events.publish("session:new", time);
        return time;
      };

      Storage.prototype.hit = function() {
        return this.add((new Date).getTime(), 'hit');
      };

      Storage.prototype.old = function() {
        var last;
        last = this.get('hit');
        if (!last) {
          return;
        }
        return (new Date).getTime() - last;
      };

      return Storage;

    })(App.Sdk.Storage);
    return Sessions.Manager = (function() {
      function Manager() {
        this.session = new Sessions.Storage;
      }

      Manager.prototype.getSessionTime = function(timeout) {
        var age, current;
        age = this.session.old();
        current = (new Date).getTime();
        if (!age) {
          return this.session.create(current);
        }
        if (age <= timeout) {
          this.session.hit();
          return this.session.getLastSession();
        } else {
          return this.session.create(current);
        }
      };

      return Manager;

    })();
  });

  this.Avantea.module('Statistics', function(Statistics, App, _, events) {
    return Statistics.Api = (function() {
      Api.prototype.lastHit = 0;

      function Api(options) {
        this.options = options;
        if (!this.options.code) {
          throw new Error("Param code is required");
        }
        if (!this.options.apiKey) {
          throw new Error("Param apiKey is required");
        }
        this.dispatcher = new App.Sdk.Dispatcher('statistics_request_', this.options.apiKey);
        this.settings = new Statistics.Options;
        this.manager = new Statistics.Sessions.Manager;
        this.interval = setInterval(_.bind(this.checkServerAndSend, this), 20 * 1000);
        events.subscribe("session:new", _.bind(this.reloadOptions, this));
      }

      Api.prototype.reloadOptions = function() {
        var promise, url;
        url = Statistics.Routes.options(this.options.code);
        promise = this.dispatcher.request('POST', url, {});
        promise.done((function(_this) {
          return function(xhr) {
            return _this.settings.assign(JSON.parse(xhr.responseText));
          };
        })(this));
        return promise;
      };

      Api.prototype.serverStatus = function() {
        var url;
        url = Statistics.Routes.status();
        return this.dispatcher.request('GET', url);
      };

      Api.prototype.checkServerAndSend = function() {
        var promise;
        if (!this.dispatcher.ableToDispatch()) {
          return;
        }
        promise = this.serverStatus();
        return promise.done(_.bind(this.dispatcher.dispatch, this.dispatcher));
      };

      Api.prototype.enter = function(slide) {
        var counted, current, event, options, session, url;
        url = Statistics.Routes.event(this.options.code, 'enter');
        current = (new Date).getTime();
        options = this.settings.getAll();
        counted = (current - this.lastHit) > options.debounce;
        session = this.manager.getSessionTime(options.timeout);
        this.lastHit = current;
        event = this.buildEvent({
          identifier: slide,
          counted: counted,
          session: session
        });
        return this.dispatcher.addToQueue('POST', url, event);
      };

      Api.prototype.buildEvent = function(object) {
        return {
          session: object.session,
          device: {
            agent: window.navigator.userAgent
          },
          event: {
            time: (new Date).getTime(),
            counted: object.counted,
            identifier: object.identifier
          }
        };
      };

      return Api;

    })();
  });

  this.Avantea.module("Mails", function(Mails, App, _, events) {
    return Mails.Routes = (function(_super) {
      __extends(Routes, _super);

      function Routes() {
        return Routes.__super__.constructor.apply(this, arguments);
      }

      Routes.service = "maily";

      Routes.mails = function() {
        return this.api(this.service, 'mails');
      };

      Routes.status = function() {
        return this.host(this.service) + 'status';
      };

      return Routes;

    })(App.Sdk.Routes);
  });

  this.Avantea.module('Mails', function(Mails, App, _, events) {
    return Mails.Api = (function() {
      Api.prototype.lastHit = 0;

      function Api(options) {
        this.options = options;
        if (!this.options.apiKey) {
          throw new Error("Param apiKey is required");
        }
        this.dispatcher = new App.Sdk.Dispatcher('mails_request_', this.options.apiKey);
        this.interval = setInterval(_.bind(this.checkServerAndSend, this), 20 * 1000);
      }

      Api.prototype.serverStatus = function() {
        return this.dispatcher.request('GET', Mails.Routes.status());
      };

      Api.prototype.checkServerAndSend = function() {
        var promise;
        if (!this.dispatcher.ableToDispatch()) {
          return;
        }
        promise = this.serverStatus();
        return promise.done(_.bind(this.dispatcher.dispatch, this.dispatcher));
      };

      Api.prototype.send = function(mail, opts) {
        var payload;
        payload = this.buildMail(mail, opts);
        return this.dispatcher.request('POST', Mails.Routes.mails(), payload);
      };

      Api.prototype.save = function(mail, opts) {
        var payload;
        payload = this.buildMail(mail, opts);
        return this.dispatcher.addToQueue('POST', Mails.Routes.mails(), payload);
      };

      Api.prototype.buildMail = function(mail, opts) {
        var object;
        object = {
          code: mail,
          time: new Date().getTime()
        };
        return _.extend(object, opts);
      };

      return Api;

    })();
  });

}).call(this);
