Avantea SDK
===========

**Kompilace:**

kompilaci je možné provést pomocí `sh compile`. Tento script spustí instalaci node modulů, pustí testy a
precompile proces. Výsledek pak můžete najít ve složce build pod názvem dist.min.js.

**Build složka:**

 - depencencies.js: soubor obsahující concatnuté závislosit, v tuto chvíli sdk je závisla na deferred, underscore
   PubSubJs a simpleStorage.

 - source.js: obsahuje nedistribuovanou verzi avantea sdk, bez závislotí. Hodí se pokud některou ze závislotí již máte
   v projektu.

 - dist.js: je sdk se závislotmi concatovaná do jednoho souboru. Neminimalizovaná

 - dist.min.js: to samé jako dist.js ovšem minimalizovaná.


**Vývoj:**

pokud se rozhodnete vyvýjet sdk, je doporučeno využít skd developerské nástroje.

`App.development = true` - direktiva development začně posílat data na localhost:3000

`grunt` - překompiluje coffee soubory dou source

`grunt specs` - spustí veškeré testy

`grunt specs:<module>` - spustí testy daného modulu

`grunt watch:source` - ohlídá změnu souboru a pokud bude soubor .coffee zmenen zkompiluje

`grunt watch:specs:<module>` - spustí testy daného modulu, moduly jsou prozatím extendable a statistics


Statistiky modul:
=================

sdk obsahuje modul pro statistiky. Samotné použití se najde v samostatných statistikách.
constructor akceptuje paramter objektu, objekt musí obsahovat
  - apiKey - autorizuje uživatele
  - code - kód prezentace

```
<script type="text/javascript" src="/path/to/sdk.js"></script>
<script type="text/javascript">
  window.slide = new Avantea.Statistics.Api({apiKey: '...', code: '...'});
</script>
```

**Metody**:

  - enter (<identifikátor slidu>): Pokud chcete dát vedět že uživatel vstoupil na daný slide invokněte enter methodu.
    Enter metoda automaticky synchronizuje data i v offline režimu.

    Příklad použití:

    ```
        slide.enter('home')
    ```


Maily modul:
=================

sdk obsahuje modul pro maily. Samotné použití se najde v samostatných mailech.
constructor akceptuje paramter objektu, objekt musí obsahovat
  - apiKey - autorizuje uživatele

```
<script type="text/javascript" src="/path/to/sdk.js"></script>
<script type="text/javascript">
  window.mail = new Avantea.Mails.Api({apiKey: '...'});
</script>
```


**Metody**:

  - send (<code mailu>, <nastavení>): Invokne odeslání emailu. Returnuje deferred (stejné jako jQuery 1.5+).
    Odešle direktně email. Nefunguje offline, nicméně returnuje deferred ze kterého se dá vyčíst odpoveď
    i to jestli mail odešel v pořádku.

  - save (<code mailu>, <nastavení>): Invokne odeslání emailu. Return je void. Není třeba být online, jde použít
    offline včetně nastavení.


**Nastavení:**

  - receivers: (Array) - pokud nemáte v šabloně statické adresy, nezbytný parametr. Seznam emailových adres jako array.

  - attachments: (Object / Hash) - objekt, který doplňuje mail o přílohy. Maximálně 3 a do 10 mb. Příklad:
    `slide.send('revolade', {attachments: {"jméno přílohy": "http://example.com/file.png"}})`

  - template_vars: (Object / Hash) - objekt, který umožňuje doplnit data pro šablonovací systém. Pro šablonování se
    používá Liquid (https://github.com/Shopify/liquid). Příklad:

    V sdk:
    ```
        slide.send('revolade', {template_vars: {thanks_to: ['Kryštof', 'Jirka']}})
    ```

    V šabloně:
    ```
    Poděkovat:
        {% for name in thanks_to %}
            {{name}}
        {% endfor %}
    ```

  - subject: (String) - přepíše předmět emailu

  - from: (String - Email) - přepíše "od" emailu

  - reply_to: (String - Email) - přepíše "odpoveďet na" emailu


