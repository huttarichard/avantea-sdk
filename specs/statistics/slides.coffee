

describe "Statistics API", ->

  slides = undefined
  xhrPrototype = Avantea.Sdk.XHR.prototype

  beforeEach ->
    slides = new Avantea.Statistics.Api(code: "code", apiKey: "apikey")
    spyOn(xhrPrototype, 'send').and.returnValue(new Deferred);
    simpleStorage.flush()


  it "should be defined", ->
    expect(Avantea?.Statistics?.Api?.prototype).toBeDefined()


  it "should be able to create new instance of slides", ->
    expect(-> new Avantea.Statistics.Api).toThrowError()
    expect(-> new Avantea.Statistics.Api(code: "code")).toThrowError()
    expect(-> new Avantea.Statistics.Api(code: "code", apiKey: "apikey")).not.toThrowError()


  it "should be able to create new session", ->
    expect(slides.manager).toBeDefined()

    session_first = slides.manager.getSessionTime(0)
    expect(session_first).toBeDefined()

    session_second = slides.manager.getSessionTime(0)
    expect(session_second).not.toEqual(session_first)


  it "should expire session if timeout", (done) ->
    expect(slides.manager).toBeDefined()

    session_first = slides.manager.getSessionTime(300)
    expect(session_first).toBeDefined()

    setTimeout ->
      session_second = slides.manager.getSessionTime(300)
      expect(session_second).toEqual(session_first)
    , 200

    setTimeout ->
      third_second = slides.manager.getSessionTime(300)
      expect(third_second).not.toEqual(session_first)
      done()
    , 1000


  it "options should be defined", ->
    expect(slides?.options).toBeDefined()


  it "should be able to keep options", ->
    slides.options.assign(debounce: 10, timeout: 10)
    expect(_.values(slides.options.getAll())).toEqual([10,10])

    slides.options.assign(slides.options.defaults)
    expect(_.values(slides.options.getAll())).not.toEqual([10,10])


  it "should be able to create new event", ->
    original = simpleStorage.index().length
    slides.enter("test")
    result = simpleStorage.index().length > original
    expect(result).toBeTruthy()


  it "should be able to create more events", ->
    original = simpleStorage.index().length
    slides.enter("test")
    slides.enter("test")
    result = simpleStorage.index().length > original
    expect(result).toBeTruthy()


  it "should contains session device and events attributes", ->
    slides.enter("test")
    slide = _.last(slides?.dispatcher?.storage?.getAll?())
    expect(slide).toBeDefined()

    expect(slide?.method).toEqual 'POST'
    expect(slide?.payload?.device?.agent).toEqual window.navigator.userAgent
    expect(_.contains([true, false], slide?.payload?.event?.counted)).toBeTruthy()
    expect(slide?.payload?.event?.identifier).toEqual 'test'
    expect(slide?.payload?.event?.time).toBeDefined()


  it "should mark counted as false", ->
    slides.enter("test")
    slides.enter("test")

    slide = _.last(slides?.dispatcher?.storage?.getAll?())
    expect(slide).toBeDefined()
    expect(slide?.event?.counted).toBeFalsy()


  it "should mark counted as true", (done) ->
    slides.enter("test")

    setTimeout ->
      slides.enter("test")
      slide = _.last(slides?.dispatcher?.storage?.getAll?())

      expect(slide).toBeDefined()
      expect(slide?.payload?.event?.counted).toBeTruthy()
      done()

    , (slides?.options?.getAll?()?.debounce + 500) or 0
