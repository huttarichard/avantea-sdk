

describe "Sdk", ->


  it "should make namespace", ->
    expect(Avantea).toBeTruthy()


  it "should have version", ->
    expect(Avantea.VERSION).toBeTruthy()


  it "should have Deferred in App", ->
    expect(Avantea.Deferred).toBeTruthy()


  it "should have storage in App", ->
    expect(Avantea.storage).toBeTruthy()


  describe "modules", ->

    it "should make app namespace", ->
      Avantea.module("TestCase", (->))
      expect(Avantea.TestCase).toEqual {}


    it "should make app namespace in deep level", ->
      Avantea.module("TestCase.TestCase", (->))
      expect(Avantea.TestCase.TestCase).toEqual {}


    it "should be available namespace in module factory arguments", ->
      Avantea.module "TestCase.TestCase", (TestCase, App, _, events) ->
        expect(TestCase).toEqual {}
        expect(App).toEqual Avantea
        expect(_).toBeTruthy()
        expect(events).toBeTruthy()

        TestCase.testVar = true

      expect(Avantea.TestCase.TestCase.testVar)


    it "should be available to export PUBLIC API", ->
      Avantea.module "TestCase.TestCase", (TestCase, App, _, events) ->
        TestCase.testVar = true

      expect(Avantea.TestCase.TestCase.testVar).toEqual true