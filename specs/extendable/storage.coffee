
describe "Dispatcher storage", ->

  storage = new Avantea.Sdk.Storage

  beforeEach ->
    localStorage.clear()
    simpleStorage.flush()


  it "should be able to add item to storage and get it back", ->
    object = {id: _.uniqueId('test')}
    id = storage.add object
    expect(storage.get id).toEqual object


  it "should be able to get all items", ->
    storage.add {id: 1}
    storage.add {id: 2}
    storage.add {id: 3}

    expect(_(storage.getAll()).map((v) -> v.id)).toEqual [1,2,3]


  it "should be able to remove item", ->
    id = storage.add {id: 1}
    expect(storage.get id).toEqual {id: 1}
    storage.remove id
    expect(storage.getAll()).toEqual []


  it "should be able to clear all items", ->
    storage.add {id: 1}
    storage.add {id: 2}
    storage.add {id: 3}

    expect(_(storage.getAll()).map((v) -> v.id)).toEqual [1,2,3]
    storage.clear()
    expect(storage.getAll()).toEqual []


  it "should be able to iterate all items over each", ->
    id = storage.add {id: 1}

    storage.each (v, k) ->
      expect(v).toEqual {id: 1}
      expect(k).toEqual id


  it "should be able to iterate all items over map", ->
    id = storage.add {id: 1}
    mapped = storage.map (v, k) -> [v, k]
    expect(mapped).toEqual [[{id: 1}, id]]


  it "should be able to iterate all items over filter", ->
    expect(storage.filter (v, k) -> false).toEqual []