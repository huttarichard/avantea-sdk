(function() {
  describe("Statistics API", function() {
    var slides, xhrPrototype;
    slides = void 0;
    xhrPrototype = Avantea.Sdk.XHR.prototype;
    beforeEach(function() {
      slides = new Avantea.Statistics.Api({
        code: "code",
        apiKey: "apikey"
      });
      spyOn(xhrPrototype, 'send').and.returnValue(new Deferred);
      return simpleStorage.flush();
    });
    it("should be defined", function() {
      var _ref, _ref1;
      return expect(typeof Avantea !== "undefined" && Avantea !== null ? (_ref = Avantea.Statistics) != null ? (_ref1 = _ref.Api) != null ? _ref1.prototype : void 0 : void 0 : void 0).toBeDefined();
    });
    it("should be able to create new instance of slides", function() {
      expect(function() {
        return new Avantea.Statistics.Api;
      }).toThrowError();
      expect(function() {
        return new Avantea.Statistics.Api({
          code: "code"
        });
      }).toThrowError();
      return expect(function() {
        return new Avantea.Statistics.Api({
          code: "code",
          apiKey: "apikey"
        });
      }).not.toThrowError();
    });
    it("should be able to create new session", function() {
      var session_first, session_second;
      expect(slides.manager).toBeDefined();
      session_first = slides.manager.getSessionTime(0);
      expect(session_first).toBeDefined();
      session_second = slides.manager.getSessionTime(0);
      return expect(session_second).not.toEqual(session_first);
    });
    it("should expire session if timeout", function(done) {
      var session_first;
      expect(slides.manager).toBeDefined();
      session_first = slides.manager.getSessionTime(300);
      expect(session_first).toBeDefined();
      setTimeout(function() {
        var session_second;
        session_second = slides.manager.getSessionTime(300);
        return expect(session_second).toEqual(session_first);
      }, 200);
      return setTimeout(function() {
        var third_second;
        third_second = slides.manager.getSessionTime(300);
        expect(third_second).not.toEqual(session_first);
        return done();
      }, 1000);
    });
    it("options should be defined", function() {
      return expect(slides != null ? slides.options : void 0).toBeDefined();
    });
    it("should be able to keep options", function() {
      slides.options.assign({
        debounce: 10,
        timeout: 10
      });
      expect(_.values(slides.options.getAll())).toEqual([10, 10]);
      slides.options.assign(slides.options.defaults);
      return expect(_.values(slides.options.getAll())).not.toEqual([10, 10]);
    });
    it("should be able to create new event", function() {
      var original, result;
      original = simpleStorage.index().length;
      slides.enter("test");
      result = simpleStorage.index().length > original;
      return expect(result).toBeTruthy();
    });
    it("should be able to create more events", function() {
      var original, result;
      original = simpleStorage.index().length;
      slides.enter("test");
      slides.enter("test");
      result = simpleStorage.index().length > original;
      return expect(result).toBeTruthy();
    });
    it("should contains session device and events attributes", function() {
      var slide, _ref, _ref1, _ref2, _ref3, _ref4, _ref5, _ref6, _ref7, _ref8, _ref9;
      slides.enter("test");
      slide = _.last(slides != null ? (_ref = slides.dispatcher) != null ? (_ref1 = _ref.storage) != null ? typeof _ref1.getAll === "function" ? _ref1.getAll() : void 0 : void 0 : void 0 : void 0);
      expect(slide).toBeDefined();
      expect(slide != null ? slide.method : void 0).toEqual('POST');
      expect(slide != null ? (_ref2 = slide.payload) != null ? (_ref3 = _ref2.device) != null ? _ref3.agent : void 0 : void 0 : void 0).toEqual(window.navigator.userAgent);
      expect(_.contains([true, false], slide != null ? (_ref4 = slide.payload) != null ? (_ref5 = _ref4.event) != null ? _ref5.counted : void 0 : void 0 : void 0)).toBeTruthy();
      expect(slide != null ? (_ref6 = slide.payload) != null ? (_ref7 = _ref6.event) != null ? _ref7.identifier : void 0 : void 0 : void 0).toEqual('test');
      return expect(slide != null ? (_ref8 = slide.payload) != null ? (_ref9 = _ref8.event) != null ? _ref9.time : void 0 : void 0 : void 0).toBeDefined();
    });
    it("should mark counted as false", function() {
      var slide, _ref, _ref1, _ref2;
      slides.enter("test");
      slides.enter("test");
      slide = _.last(slides != null ? (_ref = slides.dispatcher) != null ? (_ref1 = _ref.storage) != null ? typeof _ref1.getAll === "function" ? _ref1.getAll() : void 0 : void 0 : void 0 : void 0);
      expect(slide).toBeDefined();
      return expect(slide != null ? (_ref2 = slide.event) != null ? _ref2.counted : void 0 : void 0).toBeFalsy();
    });
    return it("should mark counted as true", function(done) {
      var _ref, _ref1;
      slides.enter("test");
      return setTimeout(function() {
        var slide, _ref, _ref1, _ref2, _ref3;
        slides.enter("test");
        slide = _.last(slides != null ? (_ref = slides.dispatcher) != null ? (_ref1 = _ref.storage) != null ? typeof _ref1.getAll === "function" ? _ref1.getAll() : void 0 : void 0 : void 0 : void 0);
        expect(slide).toBeDefined();
        expect(slide != null ? (_ref2 = slide.payload) != null ? (_ref3 = _ref2.event) != null ? _ref3.counted : void 0 : void 0 : void 0).toBeTruthy();
        return done();
      }, ((slides != null ? (_ref = slides.options) != null ? typeof _ref.getAll === "function" ? (_ref1 = _ref.getAll()) != null ? _ref1.debounce : void 0 : void 0 : void 0 : void 0) + 500) || 0);
    });
  });

}).call(this);
