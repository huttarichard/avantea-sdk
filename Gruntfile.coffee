module.exports = (grunt) ->

  # Project configuration.
  grunt.initConfig
    pkg: grunt.file.readJSON("package.json")

    concat:
      options:
        separator: ';'

      dependencies:
        src: [
          'source/libs/deferred.js',
          'source/libs/pub_sub.js',
          'source/libs/simple_storage.js',
          'source/libs/underscore.js'
        ]

        dest: 'build/dependencies.js'

      dist:
        src: [
          'build/dependencies.js',
          'build/source.js'
        ]

        dest: 'build/dist.js'


    uglify:
      options:
        banner: "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd\") %> */\n"

      dist:
        src: ['build/dist.js']
        dest: "build/dist.min.js"


    coffee:
      options:
        join: true

      source:
        files:
          'build/source.js': [
            'source/sdk.coffee',
            'source/xhr.coffee',
            'source/storage.coffee',
            'source/dispatcher.coffee',
            'source/events.coffee',
            'source/routes.coffee',
            'source/statistics/routes.coffee',
            'source/statistics/options.coffee',
            'source/statistics/sessions.coffee',
            'source/statistics/api.coffee',
            'source/mails/routes.coffee',
            'source/mails/api.coffee'
          ]

      specs_extendable:
        files:
          'specs/build/specs.js': ['specs/extendable/**/*.coffee']

      specs_statistics:
        files:
          'specs/build/specs.js': ['specs/statistics/**/*.coffee']


    jasmine:
      specs:
        src: ['build/source.js']
        options:
          specs: 'specs/build/specs.js'
          outfile: 'specs/build/specs.html'
          vendor: ["source/libs/*.js"]
          keepRunner: true


    watch:
      options:
        debounceDelay: 250
        spawn: false

      source:
        files: ['source/**/*.coffee']
        tasks: ['default']

      specs_extendable:
        files: ['specs/extendable/**/*.coffee']
        tasks: ['specs:extendable']

      specs_statistics:
        files: ['specs/statistics/**/*.coffee']
        tasks: ['specs:statistics']




  # Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks "grunt-contrib"

  # Default task(s).
  grunt.registerTask "default",    ["concat:dependencies", "coffee:source"]
  grunt.registerTask "precompile", ["concat:dependencies", "default", "concat:dist", "uglify:dist"]
  grunt.registerTask "specs", ["specs:extendable", "specs:statistics"]

  grunt.registerTask "specs:extendable", ["coffee:source", "coffee:specs_extendable", "jasmine:specs"]
  grunt.registerTask "specs:statistics", ["coffee:source", "coffee:specs_statistics", "jasmine:specs"]

  grunt.registerTask "watch:specs:extendable", ["watch:specs_extendable"]
  grunt.registerTask "watch:specs:statistics", ["watch:specs_statistics"]