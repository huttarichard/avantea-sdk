@Avantea = do (simpleStorage, PubSub, _, Deferred) ->

  App = {}

  App.VERSION = '0.0.2'

  App.development = false

  App.Deferred = Deferred

  App.storage = simpleStorage

  App.events = PubSub

  App.stringNamespace = (ns) ->
    o = undefined
    n = ns.split(".")
    o = App[n[0]] = App[n[0]] or {}
    l = n.length
    i = 1

    while i < l
      o = o[n[i]] = o[n[i]] or {}
      i++
    o


  App.module = (name, fn) ->
    namespace = if name.split('.').length > 1
      App.stringNamespace name
    else
      App[name] or= {}
      App[name]

    fn.call App, namespace, App, _, PubSub

  App
