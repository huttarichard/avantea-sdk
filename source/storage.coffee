@Avantea.module "Sdk", (Sdk, App, _, events) ->

  class Sdk.Storage

    storage: App.storage

    # expires in 90 days
    expires: 1000 * 60 * 60 * 24 * 90

    prefix: 'id'

    generateUniqueTimeId: ->
      _.uniqueId(@prefix) + (new Date).getTime()


    isCorrectPrefix: (key) ->
      key.slice(0, @prefix.length) is @prefix


    add: (obj, id) ->
      id = if id then @prefix + id else @generateUniqueTimeId()

      @storage.flush() unless @storage.canUse()
      @storage.set id, obj, TTL: @expires
      id.slice @prefix.length, id.length


    get: (id) ->
      @storage.get @prefix + id


    getAll: ->
      @map()


    remove: (key) ->
      @storage.deleteKey @prefix + key


    clear: ->
      @storage.flush()


    count: ->
      @getAll().length


    saveClear: ->
      @each (val, key) =>
        @remove key if @isCorrectPrefix key


    iterate: (iterateFn, callback = (val, key) -> val) ->
      isCorrectPrefix = _.bind @isCorrectPrefix, @
      index = _.filter @storage.index(), isCorrectPrefix

      _(index)[iterateFn] (key) =>
        notPrefixed = key.slice @prefix.length, key.length
        callback @storage.get(key), notPrefixed


    each: (callback) ->
      @iterate "each", callback


    map: (callback) ->
      @iterate "map", callback


    filter: (callback) ->
      @iterate "filter", callback



