@Avantea.module 'Statistics', (Statistics, App, _, events) ->

  class Statistics.Api

    lastHit: 0

    constructor: (@options) ->
      throw new Error("Param code is required") unless @options.code
      throw new Error("Param apiKey is required") unless @options.apiKey

      @dispatcher = new App.Sdk.Dispatcher 'statistics_request_', @options.apiKey
      @settings = new Statistics.Options
      @manager = new Statistics.Sessions.Manager

      @interval = setInterval _.bind(@checkServerAndSend, @), 20 * 1000
      events.subscribe "session:new", _.bind @reloadOptions, @


    reloadOptions: ->
      url = Statistics.Routes.options @options.code

      promise = @dispatcher.request 'POST', url, {}
      promise.done (xhr) =>
        @settings.assign JSON.parse xhr.responseText
      promise


    serverStatus: ->
      url = Statistics.Routes.status()
      @dispatcher.request 'GET', url


    checkServerAndSend: ->
      return unless @dispatcher.ableToDispatch()

      promise = @serverStatus()
      promise.done _.bind @dispatcher.dispatch, @dispatcher


    enter: (slide) ->
      url = Statistics.Routes.event @options.code, 'enter'
      
      current = (new Date).getTime()
      options = @settings.getAll()
      counted = (current - @lastHit) > options.debounce
      session = @manager.getSessionTime options.timeout

      @lastHit = current

      event = @buildEvent
        identifier: slide
        counted: counted
        session: session

      @dispatcher.addToQueue 'POST', url, event


    buildEvent: (object) ->
      session: object.session
      device:
        agent: window.navigator.userAgent
      event:
        time: (new Date).getTime()
        counted: object.counted
        identifier: object.identifier