@Avantea.module "Statistics.Sessions", (Sessions, App, _, events) ->

  class Sessions.Storage extends App.Sdk.Storage

    prefix: 'statistics_session_'

    getLastSession: ->
      @get 'cache'


    create: (time) ->
      @add time, 'cache'
      @add time, 'hit'
      events.publish "session:new", time
      time


    hit: ->
      @add (new Date).getTime(), 'hit'


    old: ->
      last = @get('hit')
      return unless last
      (new Date).getTime() - last



  class Sessions.Manager

    constructor: ->
      @session = new Sessions.Storage


    getSessionTime: (timeout) ->
      age = @session.old()
      current = (new Date).getTime()

      return @session.create current unless age

      if age <= timeout
        @session.hit()
        @session.getLastSession()
      else
        @session.create current
