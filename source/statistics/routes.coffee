@Avantea.module "Statistics", (Statistics, App, _, events) ->


  class Statistics.Routes extends App.Sdk.Routes

    @service: "statistiky"

    @event: (subject, event) ->
      @api @service, subject + "/event/" + event


    @options: (subject) ->
      @api @service, subject + "/options"


    @status: ->
      @host(@service) + 'status'


