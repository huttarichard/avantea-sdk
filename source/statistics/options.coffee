@Avantea.module "Statistics", (Statistics, App, _, events) ->

  class Statistics.Options extends App.Sdk.Storage

    prefix: 'statistics_option_'

    defaults:
      debounce: 1000
      timeout: 600000


    assign: (json) ->
      _.each @defaults, (val, key) =>
        return unless json[key]
        @add json[key], key


    getAll: ->
      options = {}
      _.each @defaults, (val, key) =>
        options[key] = @get(key) or val
      options