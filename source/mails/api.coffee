@Avantea.module 'Mails', (Mails, App, _, events) ->

  class Mails.Api

    lastHit: 0

    constructor: (@options) ->
      throw new Error("Param apiKey is required") unless @options.apiKey

      @dispatcher = new App.Sdk.Dispatcher 'mails_request_', @options.apiKey
      @interval = setInterval _.bind(@checkServerAndSend, @), 20 * 1000


    serverStatus: ->
      @dispatcher.request 'GET', Mails.Routes.status()


    checkServerAndSend: ->
      return unless @dispatcher.ableToDispatch()

      promise = @serverStatus()
      promise.done _.bind @dispatcher.dispatch, @dispatcher


    send: (mail, opts) ->
      payload = @buildMail mail, opts
      @dispatcher.request 'POST', Mails.Routes.mails(), payload


    save: (mail, opts) ->
      payload = @buildMail mail, opts
      @dispatcher.addToQueue 'POST', Mails.Routes.mails(), payload


    buildMail: (mail, opts) ->
      object =
        code: mail
        time: new Date().getTime()

      _.extend object, opts