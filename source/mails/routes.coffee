@Avantea.module "Mails", (Mails, App, _, events) ->


  class Mails.Routes extends App.Sdk.Routes

    @service: "maily"

    @mails: ->
      @api @service, 'mails'


    @status: ->
      @host(@service) + 'status'


