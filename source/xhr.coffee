@Avantea.module "Sdk", (Sdk, App, _, events) ->

  class Sdk.XHR

    constructor: (method, url) ->
      @xhr = new XMLHttpRequest
      @deferred = new Deferred

      resolve = _.bind @deferred.resolve, @deferred, @xhr
      reject = _.bind @deferred.reject, @deferred, @xhr

      @xhr.open method, url, true
      @xhr.addEventListener "load", (event) ->
        xhr = event.currentTarget
        if _.contains([200, 201, 422], xhr.status) then resolve(xhr)
        else reject(xhr)

      @xhr.addEventListener "error", reject
      @xhr.addEventListener "abort", reject


    withTokenAuth: (key) ->
      @xhr.setRequestHeader 'Authorization', 'Token token="' + key + '"'


    send: ->
      @xhr.send @data
      @deferred


    withJsonData: (data) ->
      @xhr.setRequestHeader 'Content-Type', 'application/json'
      @data = JSON.stringify data


    withData: (data) ->
      @data = data
