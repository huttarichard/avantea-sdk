@Avantea.module "Sdk", (Sdk, App, _, events) ->

  class Sdk.RequestStorage extends Sdk.Storage


  class Sdk.Dispatcher

    constructor: (namespace, @apiKey) ->
      @storage = new Sdk.RequestStorage
      @storage.prefix = namespace


    addToQueue: (method, url, payload) ->
      @storage.add {method: method, url: url, payload: payload}


    request: (method, url, payload) ->
      @send {method: method, url: url, payload: payload}


    ableToDispatch: ->
      @storage.count() and window.navigator.onLine


    dispatch: ->
      return unless @ableToDispatch()
      events.publish 'dispatcher:start'

      flushed = @storage.map (val, key) =>
        deferred = @send val
        deferred.done _.bind @storage.remove, @storage, key
        deferred

      deferred = App.Deferred.when.apply App.Deferred, flushed
      deferred.done _.bind events.publish, events, 'dispatcher:end'

      undefined


    send: (obj) ->
      xhr = new Sdk.XHR obj.method, obj.url
      xhr.withTokenAuth @apiKey
      xhr.withJsonData obj.payload
      xhr.send()