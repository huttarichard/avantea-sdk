@Avantea.module "Sdk", (Sdk, App, _, events) ->

  class Sdk.Routes

    @host: (service, protocol = "http") ->
      return "http://localhost:3000/" if App.development
      protocol + "://" + service + ".avantea.cz/"


    @api: (service, uri, version = "v1") ->
      @host(service) + "api/" + version + "/" + uri
