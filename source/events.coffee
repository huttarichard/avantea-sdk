@Avantea.module "Sdk", (Sdk, App, _, events) ->


  Sdk.onlineCallback = ->
    events.publish "device:online"


  Sdk.offlineCallback = ->
    events.publish "device:offline"


  Sdk.loadCallback = ->
    events.publish "device:load"


  Sdk.loadCallback = _.throttle Sdk.loadCallback, 200


  window.addEventListener "load", Sdk.loadCallback
  window.addEventListener 'online', Sdk.onlineCallback
  window.addEventListener 'offline', Sdk.offlineCallback

  document.addEventListener "deviceready", Sdk.loadCallback
  document.addEventListener "online", Sdk.onlineCallback
  document.addEventListener "offline", Sdk.offlineCallback



